<?php
	//require_once("Debito.class.php");
	//require_once("Credito.class.php");
	class Conta {
		private $valor;
		private $numero;
		private $id;
		private $id_user;
		//public $credito = new Credito();
		//public $debito = new Debito();

		public function getId_usuario(){
			return $this->id_user;
		}
		public function setId_usuario($iu){
			$this->id_user = (isset($iu)) ? $iu : NULL;	
		}
		public function getId(){
			return $this->id;
		}
		public function setId($i){
			$this->id = (isset($i)) ? $i : NULL;	
		}
		public function getValor(){
			return $this->valor;
		}
		public function setValor($v){
			$this->valor = (isset($v)) ? $v : NULL;	
		}
		public function getNumero(){
			return $this->numero;
		}
		public function setNumero($n){
			$this->numero = (isset($n)) ? $n : NULL;	
		}
	}

?>