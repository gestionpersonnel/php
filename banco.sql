create table Conta(
    numero varchar (30) not null,
    valor float not null,
    id int auto_increment not null primary key);

create table Cartao(
    numero varchar (30) not null,
    nome varchar (30) not null,
    id int auto_increment not null primary key);

create table Usuario(
    nome varchar (30) not null,
    senha varchar (20) not null,
    email varchar (30) not null,
    id int auto_increment not null primary key);

alter table usuario add column id_conta int;

alter table usuario add foreign key (id_conta) references conta(id);

create table Debito(
    id int auto_increment not null primary key);

alter table debito add column id_cartao int;

alter table debito add foreign key (id_cartao) references cartao(id);

create table Credito(
    prazo varchar (30) not null,
    limite float not null,
    id int auto_increment not null primary key);

alter table credito add column id_cartao int;

alter table credito add foreign key (id_cartao) references cartao(id);

 alter table cartao add column id_conta int;

alter table cartao add foreign key (id_conta) references conta(id);

 create table Corrente(
    taxa float not null,
    id int auto_increment not null primary key);

alter table corrente add column id_conta int;

alter table corrente add foreign key (id_conta) references conta(id);

create table Poupanca(
    id int auto_increment not null primary key);

alter table poupanca add column id_conta int;

alter table poupanca add foreign key (id_conta) references conta(id);

create table Sacar(
    valor float not null,
    id int not null auto_increment primary key,
    data varchar (20) not null);

alter table sacar add column id_conta int;

alter table sacar add foreign key (id_conta) references conta(id);

create table Depositar(
    valor float not null,
    id int not null auto_increment primary key,
    data varchar (20) not null);

alter table depositar add column id_conta int;

alter table depositar add foreign key (id_conta) references conta(id);

create table Compra(
    produto varchar (30) not null,
    cartao varchar (20) not null,
    valor float not null,
    id int not null auto_increment primary key);

alter table compra add column id_conta int;

alter table compra add foreign key (id_conta) references conta(id);





