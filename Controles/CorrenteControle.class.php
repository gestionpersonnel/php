<?php
	require_once("Conexao.class.php");
	require_once("../Modelos/Corrente.class.php");
	final class CorrenteControle{
		public function inserir($corrente){
			$conexao= new Conexao("../Modelos/mysql.ini");
			$sql="INSERT INTO Corrente(taxa,id_conta,id) VALUES (:taxa,:idConta,:id)";
			$comando= $conexao->getConexao()->prepare($sql);
			$taxa = $corrente->getTaxa();
			$idConta = $credito->getId_conta();
			$id = $credito->getId();
			$comando->bindParam("taxa",$taxa);
			$comando->bindParam("idConta",$idConta);
			$comando->bindParam("id",$id);
			if ($comando->execute()){
				$conexao->__destruct();
				return true;
			}else{
				return false;
			}
		}
		public function atualizar($corrente){
			$conexao=new Conexao("../Modelos/mysql.ini");
			$sql="UPDATE Corrente SET taxa=:taxa WHERE id=:id;";
			$comando=$conexao->getConexao()->prepare($sql);
			$corrente= new Corrente();
			$comando->bindParam("taxa",$taxa->getTaxa());
			$comando->bindParam("id",$id->getId());
			$comando->execute();
			$conexao->__destruct();
		}
		public function deletar($id){
			$conexao= new Conexao("../Modelos/mysql.ini");
			$comando=$conexao->getConexao()->prepare("DELETE FROM Corrente WHERE id=:id");
			$corrente= new Corrente();
			$comando->bindParam("id",$id);
			$comando->execute();//retorno booleono; Podendo entrar em uma conexão
			$conexao->__destruct();
		}
		public function consultar(){
			$conexao= new Conexao("../Modelos/mysql.ini");
			$comando= $conexao->getConexao()->prepare("SELECT * FROM Corrente WHERE id=:id");
			$corrente= new Corrente();
			$comando->bindParam("id",$id);
			$comando->bindParam("taxa",$taxa);
			$comando->execute();
			$consulta=$comando->fetch();
			$corrente->setTaxa($consulta->taxa);
			$corrente->setId($consulta->id);
			return $corrente;
			$conexao->__destruct();

		}
		public function consultarTodos(){
			$conexao= new Conexao("../Modelos/mysql.ini");
			$comando=$conexao->getConexao()->prepare("SELECT * FROM Corrente;");
			$comando->execute();
			$resultado=$comando->fetchAll();
			$lista=[];
			foreach ($resultado as $item){
				$corrente= new Corrente();
				$corrente->setTaxa($item->taxa);
				$corrente->setId($item->id);
				array_push($lista,$corrente);

			}
			return $lista;
			$conexao->__destruct();

		}
	}


?>