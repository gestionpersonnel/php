<?php
	require_once("Conexao.class.php");
	require_once("../Modelos/Debito.class.php");
	final class DebitoControle{
		public function inserir($debito){
			$conexao= new Conexao("../Modelos/mysql.ini");
			$sql="INSERT INTO Debito(id_cartao,id_user,id) VALUES (:idCartao,:idUser,:id)";
			$comando= $conexao->getConexao()->prepare($sql);
			$idUser = $debito->getId_user();
			$idCartao = $debito->getId_cartao();
			$id = $debito->getId();
			$comando->bindParam("idUser",$idUser);
			$comando->bindParam("idCartao",$idCartao);
			$comando->bindParam("id",$id);
			if ($comando->execute()){
				$conexao->__destruct();
				return true;
			}else{
				return false;
			}
		}
		public function atualizar($debito){
			$conexao=new Conexao("../Modelos/mysql.ini");
			$sql="UPDATE Debito SET numero=:numero WHERE id=:id;";
			$comando=$conexao->getConexao()->prepare($sql);
			$debito= new Debito();
			$comando->bindParam("numero",$numero->getNumero());
			$comando->bindParam("id",$id->getId());
			$comando->execute();
			$conexao->__destruct();
		}
		public function deletar($id){
			$conexao= new Conexao("../Modelos/mysql.ini");
			$comando=$conexao->getConexao()->prepare("DELETE FROM Debito WHERE id=:id");
			$debito= new Debito();
			$comando->bindParam("id",$id);
			$comando->execute();//retorno booleono; Podendo entrar em uma conexão
			$conexao->__destruct();
		}
		public function consultar(){
			$conexao= new Conexao("../Modelos/mysql.ini");
			$comando= $conexao->getConexao()->prepare("SELECT * FROM Debito WHERE id=:id");
			$debito= new Debito();
			$comando->bindParam("numero",$numero);
			$comando->bindParam("id",$id);
			$comando->execute();
			$consulta=$comando->fetch();
			$debito->setNumero($consulta->numero);
			$debito->setId($consulta->id);
			return $debito;
			$conexao->__destruct();

		}
		public function consultarTodos(){
			$conexao= new Conexao("../Modelos/mysql.ini");
			$comando=$conexao->getConexao()->prepare("SELECT * FROM Debito;");
			$comando->execute();
			$resultado=$comando->fetchAll();
			$lista=[];
			foreach ($resultado as $item){
				$debito= new Debito();
				$debito->setNumero($item->numero);
				$debito->setId($item->id);
				array_push($lista,$debito);

			}
			return $lista;
			$conexao->__destruct();

		}
	}


?>