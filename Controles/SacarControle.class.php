<?php
	require_once("Conexao.class.php");
	require_once("../Modelos/Sacar.class.php");
	final class SacarControle{
		public function inserir($sacar){
			$conexao= new Conexao("../Modelos/mysql.ini");
			$sql="INSERT INTO Sacar(data,valor,id_user,id_conta,id) VALUES (:data,:valor,:idUser,:idConta,:id)";
			$comando= $conexao->getConexao()->prepare($sql);
			$data = $sacar->getData();
			$valor = $sacar->getValor();
			$idUser = $sacar->getId_user();
			$idConta = $sacar->getId_conta();
			$id = $sacar->getId();
			$comando->bindParam("data",$data);
			$comando->bindParam("valor",$valor);
			$comando->bindParam("idUser",$idUser);
			$comando->bindParam("idConta",$idConta);
			$comando->bindParam("id",$id);
			if ($comando->execute()){
				$conexao->__destruct();
				return true;
			}else{
				return false;
			}
		}
		
		public function deletar($id){
			$conexao= new Conexao("../Modelos/mysql.ini");
			$comando=$conexao->getConexao()->prepare("DELETE FROM Sacar WHERE id=:id");
			$sacar= new Sacar();
			$comando->bindParam("id",$id);
			$comando->execute();
			$conexao->__destruct();
		}
		
		public function consultarTodos(){
			$conexao= new Conexao("../Modelos/mysql.ini");
			$comando=$conexao->getConexao()->prepare("SELECT * FROM Sacar;");
			$comando->execute();
			$resultado=$comando->fetchAll();
			$lista=[];
			foreach ($resultado as $item){
				$sacar= new Sacar();
				$sacar->setData($item->data);
				$sacar->setValor($item->valor);
				$sacar->setId($item->id);
				array_push($lista,$sacar);

			}
			return $lista;
			$conexao->__destruct();

		}
	}


?>