<?php
	require_once("Conexao.class.php");
	require_once("../Modelos/Depositar.class.php");
	final class DepositarControle{
		public function inserir($depo){
			$conexao= new Conexao("../Modelos/mysql.ini");
			$sql="INSERT INTO Depositar(data,valor,id_user,id_conta,id) VALUES (:data,:valor,:idUser,:idConta,:id)";
			$comando= $conexao->getConexao()->prepare($sql);
			$data = $depo->getData();
			$valor = $depo->getValor();
			$idUser = $sacar->getId_user();
			$idConta = $sacar->getId_conta();
			$id = $depo->getId();
			$comando->bindParam("data",$data);
			$comando->bindParam("valor",$valor);
			$comando->bindParam("idUser",$idUser);
			$comando->bindParam("idConta",$idConta);
			$comando->bindParam("id",$id);
			if ($comando->execute()){
				$conexao->__destruct();
				return true;
			}else{
				return false;
			}
		}

		public function deletar($id){
			$conexao= new Conexao("../Modelos/mysql.ini");
			$comando=$conexao->getConexao()->prepare("DELETE FROM Depositar WHERE id=:id");
			$depo= new Depositar();
			$comando->bindParam("id",$id);
			$comando->execute();
			$conexao->__destruct();
		}

		public function consultarTodos(){
			$conexao= new Conexao("../Modelos/mysql.ini");
			$comando=$conexao->getConexao()->prepare("SELECT * FROM Depositar;");
			$comando->execute();
			$resultado=$comando->fetchAll();
			$lista=[];
			foreach ($resultado as $item){
				$depo= new Depositar();
				$depo->setData($item->data);
				$depo->setValor($item->valor);
				$depo->setId($item->id);
				array_push($lista,$depo);

			}
			return $lista;
			$conexao->__destruct();

		}
	}


?>