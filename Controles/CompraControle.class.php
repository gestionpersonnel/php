<?php
	require_once("Conexao.class.php");
	require_once("../Modelos/Compra.class.php");
	final class CompraControle{
		public function inserir($compra){
			$conexao= new Conexao("../Modelos/mysql.ini");
			$sql="INSERT INTO Compra(produto,valor,id_user,id_conta,id) VALUES (:produto,:valor,:idUser,:idConta,:id)";
			$comando= $conexao->getConexao()->prepare($sql);
			$produto = $compra->getProduto();
			$valor = $compra->getValor();
			$isUser = $compra->getId_user();
			$idConta = $compra->getId_conta();
			$id = $compra->getId();
			$comando->bindParam("produto",$produto);
			$comando->bindParam("valor",$valor);
			$comando->bindParam("idUser",$idUser);
			$comando->bindParam("idConta",$idConta);
			$comando->bindParam("id",$id);
			if ($comando->execute()){
				$conexao->__destruct();
				return true;
			}else{
				return false;
			}
		}
		public function atualizar(){
			try{
				$conexao=new Conexao("../Modelos/mysql.ini");
//				$sql="UPDATE Usuario SET nome=:nome, email=:email, senha=:senha WHERE id=:id";
				$comando=$conexao->getConexao()->prepare("UPDATE Compra SET produto=:produto, valor=:valor, cartao=:cartao WHERE id=:id");
				$compra = new Compra();
				$id = $compra->getId();
				$produto = $compra->getProduto();
				$valor = $compra->getValor();
				$cartao = $cartao->getCartao();
				$comando->bindParam(":id",$id);
				$comando->bindParam(":produto",$produto);
				$comando->bindParam(":valor",$valor);
				$comando->bindParam(":cartao",$cartao);
				if ($comando->execute()){
					$conexao->__destruct();
					return true;
				}else{
					return false;
				}
//				$comando->execute();
//				$conexao->__destruct();
			}catch(PDOException $e){
				echo $e->getMessage();

			}
		}
		public function deletar($id){
			$conexao= new Conexao("../Modelos/mysql.ini");
			$comando=$conexao->getConexao()->prepare("DELETE FROM Compra WHERE id=:id");
			$compra= new Compra();
			$comando->bindParam("id",$id);
			$comando->execute();
			$conexao->__destruct();
		}
		public function consultar(){
			try{
				$conexao= new Conexao("../Modelos/mysql.ini");
				$comando= $conexao->getConexao()->prepare("SELECT * FROM Compra WHERE id=:id");
				//$usuario= new Usuario();
				//$comando->bindParam("id",$id);
				//$comando->bindParam("nome",$nome);
				//$comando->bindParam("email",$email);
				//$comando->bindParam("senha",$senha);
				$comando->execute();
				$consulta=$comando->fetchAll();
				$lista=[];
				foreach($consulta as $item){
					$compra= new Compra();
					$compra->setProduto($item->produto);
					$compra->setValor($item->valor);
					$compra->setCartao($item->cartao);
					$compra->setId($item->id);
					array_push($lista,$compra);
				}
				return $lista;
				$conexao->__destruct();
			}catch(PDOException $e){
				echo $e->getMessage();

			}

		}
		public function consultarTodos(){
			$conexao= new Conexao("../Modelos/mysql.ini");
			$comando=$conexao->getConexao()->prepare("SELECT * FROM Compra;");
			$comando->execute();
			$resultado=$comando->fetchAll();
			$lista=[];
			foreach ($resultado as $item){
				$compra= new Compra();
				$compra->setProduto($item->produto);
				$compra->setValor($item->valor);
				$compra->setCartao($item->cartao);
				$compra->setId($item->id);
				array_push($lista,$compra);

			}
			return $lista;
			$conexao->__destruct();

		}
	}


?>