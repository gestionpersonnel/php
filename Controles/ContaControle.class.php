<?php
	require_once("Conexao.class.php");
	require_once("../Modelos/Conta.class.php");
	final class ContaControle{
		public function atualizarValor($co){
			$conexao = new Conexao("../Modelos/mysql.ini");
			$sql = "UPDATE Conta SET valor=:valor WHERE id=:id";
			$comando = $conexao->getConexao()->prepare($sql);
			$valor = $co->getValor();
			$id = $co->getId();
			$comando->bindParam(":valor", $valor);
			$comando->bindParam(":id", $id);
			if($comando->execute()){
				$conexao->__destruct();
				return true;
			}else{
				return false;
			}
		//	$comando->execute();
		//	$conexao->__destruct();
		//	$saldo = NULL;
		//	if($saldo != NULL){
		//		$saldo = $resultado->saldo;
		//	}
		//	return $saldo;
		}
		public function inserir($conta){
			$conexao= new Conexao("../Modelos/mysql.ini");
			$sql="INSERT INTO Conta(valor,numero,id) VALUES (:valor,:numero,:id)";
			$comando= $conexao->getConexao()->prepare($sql);
			$valor = $conta->getValor();
			$numero = $conta->getNumero();
			$id = $conta->getId();
			$comando->bindParam("valor",$valor);
			$comando->bindParam("numero",$numero);
			$comando->bindParam("id",$id);
			if ($comando->execute()){
				$conexao->__destruct();
				return true;
			}else{
				return false;
			}
		}
		public function atualizar($conta){
			$conexao=new Conexao("../Modelos/mysql.ini");
			$sql="UPDATE Conta SET valor=:valor, numero=:numero WHERE id=:id;";
			$comando=$conexao->getConexao()->prepare($sql);
			$conta= new Conta();
			$comando->bindParam("valor",$valor->getValor());
			$comando->bindParam("numero",$numero->getNumero());
			$comando->bindParam("id",$id->getId());
			$comando->execute();
			$conexao->__destruct();
		}
		public function deletar($id){
			$conexao= new Conexao("../Modelos/mysql.ini");
			$comando=$conexao->getConexao()->prepare("DELETE FROM Conta WHERE id=:id");
			$conta= new Conta();
			$comando->bindParam("id",$id);
			$comando->execute();
			$conexao->__destruct();
		}
		public function consultar(){
			$conexao= new Conexao("../Modelos/mysql.ini");
			$comando= $conexao->getConexao()->prepare("SELECT * FROM Conta WHERE id=:id");
			$conta= new Conta();
			$comando->bindParam("id",$id);
			$comando->bindParam("valor",$valor);
			$comando->bindParam("numero",$numero);
			$comando->execute();
			$consulta=$comando->fetch();
			$conta->setValor($consulta->valor);
			$conta->setNumero($consulta->numero);
			$conta->setId($consulta->id);
			return $conta;
			$conexao->__destruct();

		}
		public function consultarTodos(){
			$conexao= new Conexao("../Modelos/mysql.ini");
			$comando=$conexao->getConexao()->prepare("SELECT * FROM Conta;");
			$comando->execute();
			$resultado=$comando->fetchAll();
			$lista=[];
			foreach ($resultado as $item){
				$conta= new Conta();
				$conta->setValor($item->valor);
				$conta->setNumero($item->numero);
				$conta->setId($item->id);
				array_push($lista,$conta);

			}
			return $lista;
			$conexao->__destruct();

		}
		public function cunsultarNumeroConta($nConta){
			$conexao= new Conexao("../Modelos/mysql.ini");
			$comando=$conexao->getConexao()->prepare("SELECT * FROM Conta WHERE numero=:numero");
			$comando->bindParam("numero",$nConta);
			$comando->execute();
			$resultado = $comando->fetchAll();
			$lista=[];
			foreach($resultado as $item){
				$conta= new Conta();
				$conta->setValor($item->valor);
				$conta->setNumero($item->numero);
				$conta->setId($item->id);
				array_push($lista,$conta);
			}
			return $lista;
			$conexao->__destruct();
		}
		public function consultarValor($idUser){
			$conexao= new Conexao("../Modelos/mysql.ini");
			$comando=$conexao->getConexao()->prepare("SELECT valor FROM Conta WHERE id_user=:idUser");
			$comando->bindParam("idUser",$idUser);
			$comando->execute();
			$resultado = $comando->fetchAll();
			$lista=[];
			foreach($resultado as $item){
				$conta= new Conta();
				$conta->setValor($item->valor);
				$conta->setId_user($item->idUser);
				array_push($lista,$conta);
			}
			return $lista;
			$conexao->__destruct();
		}
		public function cunsultarId($idUser){
			$conexao= new Conexao("../Modelos/mysql.ini");
			$comando=$conexao->getConexao()->prepare("SELECT id FROM Conta WHERE id_user=:idUser");
			$comando->bindParam("idUser",$idUser);
			$comando->execute();
			$resultado = $comando->fetchAll();
			$lista=[];
			foreach($resultado as $item){
				$conta= new Conta();
				$conta->setId($item->id);
				array_push($lista,$conta);
			}
			return $lista;
			$conexao->__destruct();
		}
	}


?>