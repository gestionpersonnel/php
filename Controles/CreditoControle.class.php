<?php
	require_once("Conexao.class.php");
	require_once("../Modelos/Credito.class.php");
	final class CreditoControle{
		public function inserir($credito){
			$conexao= new Conexao("../Modelos/mysql.ini");
			$sql="INSERT INTO Credito(prazo,limite,id_cartao,id_user,id) VALUES (:prazo,:limite,:idCartao,:idUser,:id)";
			$comando= $conexao->getConexao()->prepare($sql);
			$prazo = $credito->getPrazo();
			$limite = $credito->getLimite();
			$id = $credito->getId();
			$idUser = $credito->getId_user();
			$idCartao = $credito->getId_cartao();
			$comando->bindParam("prazo",$prazo);
			$comando->bindParam("limite",$limite);
			$comando->bindParam("idUser",$idUser);
			$comando->bindParam("idCartao",$idCartao);
			$comando->bindParam("id",$id);
			if ($comando->execute()){
				$conexao->__destruct();
				return true;
			}else{
				return false;
			}
		}
		public function atualizar($credito){
			$conexao=new Conexao("../Modelos/mysql.ini");
			$sql="UPDATE Credito SET prazo=:prazo, limite=:limite, numero=:numero WHERE id=:id;";
			$comando=$conexao->getConexao()->prepare($sql);
			$credito= new Credito();
			$comando->bindParam("prazo",$prazo->getPrazo());
			$comando->bindParam("limite",$limite->getLimite());
			$comando->bindParam("numero",$numero->getNumero());
			$comando->bindParam("id",$id->getId());
			$comando->execute();
			$conexao->__destruct();
		}
		public function deletar($id){
			$conexao= new Conexao("../Modelos/mysql.ini");
			$comando=$conexao->getConexao()->prepare("DELETE FROM Credito WHERE id=:id");
			$credito= new Credito();
			$comando->bindParam("id",$id);
			$comando->execute();//retorno booleono; Podendo entrar em uma conexão
			$conexao->__destruct();
		}
		public function consultar(){
			$conexao= new Conexao("../Modelos/mysql.ini");
			$comando= $conexao->getConexao()->prepare("SELECT * FROM Credito WHERE id=:id");
			$credito= new Credito();
			$comando->bindParam("id",$id);
			$comando->bindParam("prazo",$prazo);
			$comando->bindParam("limite",$limite);
			$comando->bindParam("numero",$numero);
			$comando->execute();
			$consulta=$comando->fetch();
			$credito->setPrazo($consulta->prazo);
			$credito->setLimite($consulta->limite);
			$credito->setId($consulta->id);
			return $credito;
			$conexao->__destruct();

		}
		public function consultarTodos(){
			$conexao= new Conexao("../Modelos/mysql.ini");
			$comando=$conexao->getConexao()->prepare("SELECT * FROM Credito;");
			$comando->execute();
			$resultado=$comando->fetchAll();
			$lista=[];
			foreach ($resultado as $item){
				$credito= new Credito();
				$credito->setPrazo($item->prazo);
				$credito->setLimite($item->limite);
				$credito->setNumero($item->numero);
				$credito->setId($item->id);
				array_push($lista,$credito);

			}
			return $lista;
			$conexao->__destruct();

		}
	}


?>