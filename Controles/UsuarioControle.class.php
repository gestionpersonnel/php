<?php
	require_once("Conexao.class.php");
	require_once("../Modelos/Usuario.class.php");
	final class UsuarioControle{
		//public function conta(){
		//	$conexao = new Conexao("../Modelos/mysql.ini");
		//	$sql ="SELECT "
		//}
		public function login($usu){
			$conexao= new Conexao("../Modelos/mysql.ini");
			$nome = $usu->getNome();
			$senha = $usu->getSenha();
			$sql="SELECT * FROM Usuario WHERE nome=:nome AND senha=:senha";
			$comando=$conexao->getConexao()->prepare($sql);
			$comando->bindParam(":nome",$nome);
			$comando->bindParam(":senha",$senha);
			$comando->execute();
			$consulta = $comando->fetchAll();
			$lista = [];
			foreach($consulta as $item){
				$usuario = new Usuario();
				$usuario->setNome($item->nome);
				$usuario->setSenha($item->senha);
				array_push($lista, $usuario);
			}
			return $lista;
			$conexao->__destruct();
		}
		public function inserir($usuario){
			$conexao= new Conexao("../Modelos/mysql.ini");
			$sql="INSERT INTO Usuario(nome,email,senha,id) VALUES (:nome,:email,:senha,:id)";
			$comando= $conexao->getConexao()->prepare($sql);
			$nome = $usuario->getNome();
			$email = $usuario->getEmail();
			$senha = $usuario->getSenha();
			$id = $usuario->getId();
			$comando->bindParam(":nome",$nome);
			$comando->bindParam(":email",$email);
			$comando->bindParam(":senha",$senha);
			$comando->bindParam(":id",$id);
			if ($comando->execute()){
				$conexao->__destruct();
				return true;
			}else{
				return false;
			}
		}
		public function atualizar($usu){
			try{
				$conexao=new Conexao("../Modelos/mysql.ini");
//				$sql="UPDATE Usuario SET nome=:nome, email=:email, senha=:senha WHERE id=:id";
				$comando=$conexao->getConexao()->prepare("UPDATE Usuario SET nome=:nome, email=:email, senha=:senha WHERE id=:id");
				$id = $usu->getId();
				$nome = $usu->getNome();
				$email = $usu->getEmail();
				$senha = $usu->getSenha();
				$comando->bindParam(":id",$id);
				$comando->bindParam(":nome",$nome);
				$comando->bindParam(":email",$email);
				$comando->bindParam(":senha",$senha);
				if ($comando->execute()){
					$conexao->__destruct();
					return true;
				}else{
					return false;
				}
//				$comando->execute();
//				$conexao->__destruct();
			}catch(PDOException $e){
				echo $e->getMessage();

			}
		}
		public function deletar($id){
			$conexao= new Conexao("../Modelos/mysql.ini");
			$comando=$conexao->getConexao()->prepare("DELETE FROM Usuario WHERE id=:id");
			$usuario= new Usuario();
			$comando->bindParam("id",$id);
			$comando->execute();
			$conexao->__destruct();
		}
		public function consultar(){
			try{
				$conexao= new Conexao("../Modelos/mysql.ini");
				$comando= $conexao->getConexao()->prepare("SELECT * FROM Usuario WHERE id=:id");
				//$usuario= new Usuario();
				$comando->bindParam("id",$id);
				//$comando->bindParam("nome",$nome);
				//$comando->bindParam("email",$email);
				//$comando->bindParam("senha",$senha);
				$comando->execute();
				$consulta=$comando->fetchAll();
				$lista= array();
				foreach($consulta as $item){
					$usuario= new Usuario();
					$usuario->setNome($item->nome);
					$usuario->setEmail($item->email);
					$usuario->setSenha($item->senha);
					$usuario->setId($item->id);
					array_push($lista,$usuario);
				}
				return $lista;
				$conexao->__destruct();
			}catch(PDOException $e){
				echo $e->getMessage();

			}

		}
		public function consultarTodos(){
			$conexao= new Conexao("../Modelos/mysql.ini");
			$comando=$conexao->getConexao()->prepare("SELECT * FROM Usuario;");
			$comando->execute();
			$resultado=$comando->fetchAll();
			$lista=[];
			foreach ($resultado as $item){
				$usuario= new Usuario();
				$usuario->setNome($item->nome);
				$usuario->setEmail($item->email);
				$usuario->setSenha($item->senha);
				$usuario->setId($item->id);
				array_push($lista,$usuario);

			}
			return $lista;
			$conexao->__destruct();

		}
	}


?>