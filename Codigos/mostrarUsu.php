<?php
require_once("../Modelos/Usuario.class.php");
require_once("../Controles/UsuarioControle.class.php");
session_start();
if ( !isset($_SESSION['nome']) and !isset($_SESSION['senha']) ) {
	session_destroy();

	unset ($_SESSION['nome']);
	unset ($_SESSION['senha']);
	
	header('Location:../index.php');
}
$mostrar = new UsuarioControle();
$a = $mostrar->consultarTodos();
echo"
<!DOCTYPE HTML>
<html>
<head>
	<meta charset='UTF-8'>
	<title>teste</title>
	<link rel='stylesheet' href='css/uikit.css'>
    <script src='js/uikit.js'></script>
    <script src='js/uikit-icons.js'></script>   
    <link rel='stylesheet' href='estilo.css'/>

</head>
<body>
<div id='bot'></div>
<div id='header'>
	<div class='uk-position-top-center'>
    <img src='../Gestion.png' style='width:100%;' alt=''>
    <div class='uk-position-bottom-left'>
    <nav class='uk-navbar-container uk-navbar-transparent' uk-navbar>
        <div class='uk-navbar-left'>
        <ul class='uk-navbar-nav'>
        <li>
        <div class='menu'>
        <a href='#'><p>Menu</p></a>
            <div class='uk-navbar-dropdown uk-navbar-dropdown-width-2'>
                <div class='uk-navbar-dropdown-grid uk-child-width-1-2' uk-grid>
                    <div class='menu2'>
                			<ul class='uk-nav uk-navbar-dropdown-nav'>
                   			<li class='uk-active'><a href='mostrarUsu.php'>Home</a></li>
					        <li ><a href='compras.php'>Adicionar compras</a></li>
					        <li ><a href='ativ.php'>Minhas compras</a></li>					        
                			</ul>
            			</div>
            			<div>
	            			<ul class='uk-nav uk-navbar-dropdown-nav'>	            					            			
					        	<li><a href='#'>Adicionar cartão</a>
						        	<div uk-dropdown='pos: right-center' class='uk-navbar-dropdown'>
						        		<ul class='uk-nav uk-navbar-dropdown-nav'>
						        			<li><a href='credito.php'>Credito</a></li>
						        			<li><a href='debito.php'>Debito</a></li>
						        		</ul>
						        	</div>
					        	</li>
	            				<li><a href='saque.php'>Saques</a></li>
					        	<li><a href='deposito.php'>Depositos</a></li>
								<li><a href='deslogar.php'>SAIR</a></li>
	            			</ul>
            			</div>
            		</div>
            	</div>
            </li>
            </ul>
           </div>
        </nav>
    </div>
 	</div>
 </div>


";



foreach ($a as $b) {
    echo"
    <tr>
		<td>{$b->getNome()}</td>
    	<td>{$b->getEmail()}</td>
    	<td>{$b->getSenha()}</td>

    	<td><a href='editarUsu.php?id={$b->getId()}'>Editar</a></td>
    	<td><a href='removerUsu.php?id={$b->getId()}'>Apagar</a></td><br>
    </tr>

    ";
}
echo"
<footer>
	<div class='uk-column-1-2 uk-column-divider'>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt 
    ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.</p>

    <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
    Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>    
</div>
</footer>


</body>
<script>
$('#bot').ready(function(){
	Swal.fire({
		  position: 'top-end',
		  type: 'success',
		  title: 'Bem-vindo',
		  showConfirmButton: false,
		  timer: 1500
		})
});
</script>

</html>
";

?>
