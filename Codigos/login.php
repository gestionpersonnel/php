<?php
session_start();
echo"
	<!DOCTYPE HTML>
	<html lang='pt-br'>
	<head>
		<title>teste</title>
		<meta charset='UTF-8'>
		<link rel='stylesheet' href='css/uikit.css'>
		<script src='js/uikit.js'></script>
		<script src='js/uikit-icons.js'></script>
		<link rel='stylesheet' href='estilo.css'/>
	</head>
	<body>
		<div id='img-bk'>	
			<center>
				<div class='uk-position-center uk-overlay uk-overlay-default'>
					<div class='uk-card uk-card-default uk-card-body'>
						<form action='fazerLogin.php' method='post'>
							<div class='uk-margin'>
		        				<div class='uk-inline'>
		            				<span class='uk-form-icon' uk-icon='icon: user'></span>
		            				<label>Nome:<br>
									<input class='uk-input uk-form-width-medium' type='text' name='nome'  required/>
								</div>
							</div>
							<div class='uk-margin'>
		        				<div class='uk-inline'>
		            				<span class='uk-form-icon uk-form-icon-flip' uk-icon='icon: lock'></span>
		            				<label>Senha:</label><br>
									<input class='uk-input uk-form-width-medium' type='password' name='senha'  required/>
								</div>
							</div>					
							<hr class='uk-divider-icon'>		
							
								<button  class='uk-button uk-button-default'>enviar</button><br>
								<a href='cadastro.php'>cadastrar-se</a>
												 
						</form>				
					</div>
				</div>
			</div>
			</center>
		</body>
		</html>
	";
?>
